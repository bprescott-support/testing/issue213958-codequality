# GitLab Smoke Tests

Use GitLab CI to quickly test if GitLab features are working as intended.
Currently supported:

* GitLab CI
  * [Job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html)
  * [Job caching](https://docs.gitlab.com/ee/ci/caching/)
* GitLab Package Registry
  * [Container Registry](https://docs.gitlab.com/ee/user/packages/container_registry/index.html)
  * [Maven Repository](https://docs.gitlab.com/ee/user/packages/maven_repository/index.html)
  * [NPM Registry](https://docs.gitlab.com/ee/user/packages/npm_registry/index.html)

Each feature's smoke test is a set of one or more CI jobs that needs to be
manually triggered by default. To run all tests automatically, set a
`RUN_ALL_TESTS` CI/CD environment variable.

## Minimum requirements

These tests need require a minimum of GitLab 12.3 as they use the [`rules`](https://docs.gitlab.com/ee/ci/yaml/#rules)
CI syntax introduced in that version.
